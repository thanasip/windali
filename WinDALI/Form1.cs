﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Drawing;

namespace WinDALI {
    public partial class Form1 : Form {
        TextBox tb;
        TcpClient tcpc = new TcpClient();
        NetworkStream ns = null;
        UdpClient udpc = new UdpClient();
        IPAddress ip;
        private static byte[] accessGranted = new byte[] { 0xE0, 0xE0, 0x00, 0x00, 0xE0, 0x00, 0xE0 };
        Form1 thisForm;
        System.Timers.Timer timer;
        System.Timers.Timer sendTimer;
        int loops = 0;
        int trackBar1Value = 0;

        public Form1() {
            InitializeComponent();
            tb = outputBox;
            thisForm = this;
            commSelect.SelectedIndex = 0;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            if (MessageBox.Show("Are you sure?", "Exit?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.Cancel) {
                e.Cancel = true;
            }
        }

        private void writeOutput(string s) {
            Invoke((MethodInvoker)delegate {
                tb.Text = (s);
            });
        }

        private void appendOutput(string s) {
            Invoke((MethodInvoker)delegate {
                tb.Text += (s);
            });
        }

        private bool checkConnection(byte[] b) {
            if (ip == null) {
                MessageBox.Show("Run discovery first!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            } else if (!tcpc.Connected && b[0] != 0xE0) {
                MessageBox.Show("eDIDIO Not Connected!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private bool checkConnection() {
            if (ip == null) {
                MessageBox.Show("Run discovery first!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            } else if (!tcpc.Connected) {
                MessageBox.Show("eDIDIO Not Connected!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void showNotification(string title, string message, ToolTipIcon icon) {
            NotifyIcon ni = new NotifyIcon();
            ni.Visible = true;
            ni.BalloonTipTitle = title;
            ni.BalloonTipText = message;
            ni.BalloonTipIcon = icon;
            ni.Icon = SystemIcons.WinLogo;
            ni.ShowBalloonTip(3000);
            ni.Dispose();
        }

        private bool compareByteArrays(byte[] a, byte[] b) {
            if (a == null || b == null || (a.Length != b.Length)) return false;
            for (int i = 0; i < a.Length; i++) if (a[i] != b[i]) return false;
            return true;
        }

        private void discovery() {
            try {
                IPEndPoint bc = new IPEndPoint(IPAddress.Broadcast, 30303);
                IPEndPoint any = new IPEndPoint(IPAddress.Any, 0);
                byte[] resp;
                string str = "";

                udpc.Send(new byte[] { (byte)'D' }, 1, bc);
                Thread.Sleep(100);

                while (udpc.Available > 0) {
                    resp = udpc.Receive(ref any);
                    foreach (byte x in resp) {
                        str = str + Convert.ToChar(x);
                    }
                    showNotification("Response from " + any.Address, str, ToolTipIcon.Info);
                    Thread.Sleep(100);
                    str = "";
                }
                
                ip = any.Address;
                //MessageBox.Show(str, "Response from " + any.Address, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //showNotification("Response from " + any.Address, str, ToolTipIcon.Info);
            } catch (Exception e) {
                MessageBox.Show(e.ToString(), "Error during Discovery!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void sendBytes(byte[] b) {
            if (checkConnection(b)) {
                try {
                    if (!tcpc.Connected) {
                        tcpc.Connect(ip, 23);
                        ns = tcpc.GetStream();
                        Invoke((MethodInvoker)delegate { thisForm.Text = "WinDALI 0.1 - Connected to: " + ip + ":23"; });
                    }
                    ns.Write(b, 0, b.Length);
                    if (b[0] == 0xE0) {
                        byte[] resp = new byte[7];
                        for (int i = 0; i < 7; i++) {
                            resp[i] = (byte)ns.ReadByte();
                            writeOutput("Sent: " + BitConverter.ToString(b) + "\r\nReceived: " + BitConverter.ToString(resp));
                            if (compareByteArrays(resp, accessGranted)) {
                                //MessageBox.Show("Connected Successfully!", "Response from eDIDIO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                showNotification("Response from eDIDIO:", "Connected Successfully!", ToolTipIcon.Info);
                            }
                        }
                    } else {
                        writeOutput("Sent: " + BitConverter.ToString(b) + "\r\n");
                    }
                } catch (Exception e) {
                    MessageBox.Show(e.ToString(), "Error sending packet!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (ns != null) ns.Close();
                    if (tcpc.Connected) tcpc.Close();
                }
                Thread.Sleep(100);
            }
        }

        private void testButton_Click(object sender, EventArgs e) {
            new Thread(() => {
                discovery();
            }).Start();
        }

        private void testButton2_Click(object sender, EventArgs e) {
            int selectedCommand = commSelect.SelectedIndex - 1;
            byte selectedAddress;
            if (addrCombo.Value == 80) {
                selectedAddress = 0xFF;
            } else {
                selectedAddress = (byte) addrCombo.Value;
            }
            if (commSelect.SelectedItem.ToString().Equals("ARC_LEVEL")) {
                new Thread(() => {
                    sendBytes(new byte[] { 0xA3,
                        0x00,
                        0x00,
                        0x00,
                        (byte) (2 * selectedAddress),
                        (byte) levelCombo.Value,
                        (byte) (0xA3 ^ (byte) (2 * selectedAddress) ^ (byte) levelCombo.Value)});
                }).Start();
            } else {
                new Thread(() => {
                    sendBytes(new byte[] { 0xA3,
                        0x00,
                        0x00,
                        0x00,
                        (byte) ((2 * selectedAddress) + 1),
                        (byte) selectedCommand,
                        (byte) (0xA3 ^ (byte) ((2 * selectedAddress) + 1) ^ (byte) selectedCommand)});
                }).Start();
            }
        }

        private void connButton_Click(object sender, EventArgs e) {
            new Thread(() => {
                sendBytes(new byte[] { 0xE0, 0x00, 0x00, 0x00, 0x00, 0xB0, (0xE0 ^ 0xB0) });
            }).Start();
        }

        private void commSelect_SelectedIndexChanged(object sender, EventArgs e) {
            if (commSelect.SelectedItem.ToString().Equals("ARC_LEVEL")) {
                levelCombo.Enabled = true;
            } else {
                levelCombo.Enabled = false;
            }
        }

        private void sequenceButton_Click(object sender, EventArgs e) {
            if (checkConnection()) {
                if (timer == null) {
                    timer = new System.Timers.Timer();
                    timer.Interval = 2000;
                    timer.Elapsed += new System.Timers.ElapsedEventHandler(Sequence);
                    timer.Enabled = true;
                    sequenceButton.Text = "Stop Sequence";
                } else {
                    timer.Enabled = false;
                    timer.Dispose();
                    timer = null;
                    sequenceButton.Text = "Start Sequence";
                    loops = 0;
                    sendBytes(new byte[] { 0xA3, 0x00, 0x00, 0x00, 0xFE, 0x00, (0xA3 ^ 0xFE ^ 0x00) });
                }
            }
        }

        private void Sequence(object sender, System.Timers.ElapsedEventArgs e) {
            switch (loops++) {
                case 0:
                    sendBytes(new byte[] { 0xA3, 0x00, 0x00, 0x00, 0x02, 0x00, (0xA3 ^ 0x02 ^ 0x00) });
                    sendBytes(new byte[] { 0xA3, 0x00, 0x00, 0x00, 0x04, 0x00, (0xA3 ^ 0x04 ^ 0x00) });
                    sendBytes(new byte[] { 0xA3, 0x00, 0x00, 0x00, 0x00, 0x20, (0xA3 ^ 0x00 ^ 0x20) });
                    break;
                case 1:
                    sendBytes(new byte[] { 0xA3, 0x00, 0x00, 0x00, 0x00, 0x00, (0xA3 ^ 0x00 ^ 0x00) });
                    sendBytes(new byte[] { 0xA3, 0x00, 0x00, 0x00, 0x04, 0x00, (0xA3 ^ 0x04 ^ 0x00) });
                    sendBytes(new byte[] { 0xA3, 0x00, 0x00, 0x00, 0x02, 0x20, (0xA3 ^ 0x02 ^ 0x20) });
                    break;
                case 2:
                    sendBytes(new byte[] { 0xA3, 0x00, 0x00, 0x00, 0x02, 0x00, (0xA3 ^ 0x02 ^ 0x00) });
                    sendBytes(new byte[] { 0xA3, 0x00, 0x00, 0x00, 0x00, 0x00, (0xA3 ^ 0x00 ^ 0x00) });
                    sendBytes(new byte[] { 0xA3, 0x00, 0x00, 0x00, 0x04, 0x20, (0xA3 ^ 0x04 ^ 0x20) });
                    loops = 0;
                    break;
            }
        }

        private void trackBar1_MouseDown(object sender, MouseEventArgs e) {
            if (ip == null) {
                MessageBox.Show("Run discovery first!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else if (!tcpc.Connected) {
                MessageBox.Show("eDIDIO Not Connected!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else {
                sendTimer = new System.Timers.Timer();
                sendTimer.Interval = 50;
                sendTimer.Elapsed += sendTimer_Elapsed;
                sendTimer.Enabled = true;
            }
        }
        
        private void trackBar1_MouseUp(object sender, MouseEventArgs e) {
            sendTimer.Enabled = false;
            sendTimer.Dispose();
            sendTimer = null;
        }

        private void updateSliderValue() {
            Invoke((MethodInvoker)delegate {
                trackBar1Value = trackBar1.Value;
            });
        }

        private void sendTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {
            int selectedAddress;
            updateSliderValue();

            if (slider1Addr.Value == 80) {
                selectedAddress = 0xFF;
            } else {
                selectedAddress = (byte) slider1Addr.Value;
            }

            new Thread(() => {
                sendBytes(new byte[] { 0xA3,
                        0x00,
                        0x00,
                        0x00,
                        (byte) (2 * selectedAddress),
                        (byte) trackBar1Value,
                        (byte) (0xA3 ^ (byte) (2 * selectedAddress) ^ (byte) trackBar1Value)});
            }).Start();
        }
    }
}
