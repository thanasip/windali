﻿namespace WinDALI {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.testButton = new System.Windows.Forms.Button();
            this.outputBox = new System.Windows.Forms.TextBox();
            this.testButton2 = new System.Windows.Forms.Button();
            this.connButton = new System.Windows.Forms.Button();
            this.commSelect = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.addrCombo = new System.Windows.Forms.NumericUpDown();
            this.levelCombo = new System.Windows.Forms.NumericUpDown();
            this.sequenceButton = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.slider1Addr = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.addrCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.levelCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slider1Addr)).BeginInit();
            this.SuspendLayout();
            // 
            // testButton
            // 
            this.testButton.Location = new System.Drawing.Point(258, 12);
            this.testButton.Name = "testButton";
            this.testButton.Size = new System.Drawing.Size(79, 21);
            this.testButton.TabIndex = 0;
            this.testButton.Text = "Discovery";
            this.testButton.UseVisualStyleBackColor = true;
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // outputBox
            // 
            this.outputBox.Location = new System.Drawing.Point(13, 66);
            this.outputBox.MaxLength = 9999999;
            this.outputBox.Multiline = true;
            this.outputBox.Name = "outputBox";
            this.outputBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputBox.Size = new System.Drawing.Size(409, 183);
            this.outputBox.TabIndex = 1;
            // 
            // testButton2
            // 
            this.testButton2.Location = new System.Drawing.Point(343, 39);
            this.testButton2.Name = "testButton2";
            this.testButton2.Size = new System.Drawing.Size(79, 21);
            this.testButton2.TabIndex = 2;
            this.testButton2.Text = "Test Packet";
            this.testButton2.UseVisualStyleBackColor = true;
            this.testButton2.Click += new System.EventHandler(this.testButton2_Click);
            // 
            // connButton
            // 
            this.connButton.Location = new System.Drawing.Point(343, 12);
            this.connButton.Name = "connButton";
            this.connButton.Size = new System.Drawing.Size(79, 21);
            this.connButton.TabIndex = 3;
            this.connButton.Text = "Connect";
            this.connButton.UseVisualStyleBackColor = true;
            this.connButton.Click += new System.EventHandler(this.connButton_Click);
            // 
            // commSelect
            // 
            this.commSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.commSelect.FormattingEnabled = true;
            this.commSelect.Items.AddRange(new object[] {
            "ARC_LEVEL",
            "OFF",
            "FADE_UP",
            "FADE_DOWN",
            "STEP_UP",
            "STEP_DOWN",
            "RECALL_MAX",
            "RECALL_MIN",
            "STEP_DOWN_AND_OFF",
            "ON_AND_STEP_UP"});
            this.commSelect.Location = new System.Drawing.Point(123, 39);
            this.commSelect.Name = "commSelect";
            this.commSelect.Size = new System.Drawing.Size(129, 21);
            this.commSelect.TabIndex = 4;
            this.commSelect.SelectedIndexChanged += new System.EventHandler(this.commSelect_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Address: ";
            // 
            // addrCombo
            // 
            this.addrCombo.Location = new System.Drawing.Point(69, 40);
            this.addrCombo.Maximum = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.addrCombo.Name = "addrCombo";
            this.addrCombo.Size = new System.Drawing.Size(48, 20);
            this.addrCombo.TabIndex = 6;
            // 
            // levelCombo
            // 
            this.levelCombo.Enabled = false;
            this.levelCombo.Location = new System.Drawing.Point(258, 40);
            this.levelCombo.Maximum = new decimal(new int[] {
            254,
            0,
            0,
            0});
            this.levelCombo.Name = "levelCombo";
            this.levelCombo.Size = new System.Drawing.Size(79, 20);
            this.levelCombo.TabIndex = 7;
            // 
            // sequenceButton
            // 
            this.sequenceButton.Location = new System.Drawing.Point(123, 12);
            this.sequenceButton.Name = "sequenceButton";
            this.sequenceButton.Size = new System.Drawing.Size(129, 21);
            this.sequenceButton.TabIndex = 8;
            this.sequenceButton.Text = "Start Sequence";
            this.sequenceButton.UseVisualStyleBackColor = true;
            this.sequenceButton.Click += new System.EventHandler(this.sequenceButton_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(123, 255);
            this.trackBar1.Maximum = 254;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(299, 45);
            this.trackBar1.TabIndex = 9;
            this.trackBar1.TickFrequency = 5;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trackBar1_MouseDown);
            this.trackBar1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trackBar1_MouseUp);
            // 
            // slider1Addr
            // 
            this.slider1Addr.Location = new System.Drawing.Point(69, 266);
            this.slider1Addr.Maximum = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.slider1Addr.Name = "slider1Addr";
            this.slider1Addr.Size = new System.Drawing.Size(48, 20);
            this.slider1Addr.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Address: ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 306);
            this.Controls.Add(this.slider1Addr);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.sequenceButton);
            this.Controls.Add(this.levelCombo);
            this.Controls.Add(this.addrCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.commSelect);
            this.Controls.Add(this.connButton);
            this.Controls.Add(this.testButton2);
            this.Controls.Add(this.outputBox);
            this.Controls.Add(this.testButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(450, 1000);
            this.MinimumSize = new System.Drawing.Size(450, 300);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WinDALI 0.1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.addrCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.levelCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slider1Addr)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button testButton;
        private System.Windows.Forms.TextBox outputBox;
        private System.Windows.Forms.Button testButton2;
        private System.Windows.Forms.Button connButton;
        private System.Windows.Forms.ComboBox commSelect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown addrCombo;
        private System.Windows.Forms.NumericUpDown levelCombo;
        private System.Windows.Forms.Button sequenceButton;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.NumericUpDown slider1Addr;
        private System.Windows.Forms.Label label2;
    }
}

